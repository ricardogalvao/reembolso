<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReembolsoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reembolso', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tipo_reembolso_id');
            $table->unsignedBigInteger('entidade_reembolso_id');
            $table->unsignedBigInteger('user_id');
            $table->string('descricao')->nullable()->default(null);
            $table->date('data');
            $table->float('valor')->nullable()->default(null);
            $table->float('valor_inicial')->nullable()->default(null);
            $table->float('valor_final')->nullable()->default(null);
            $table->foreign('tipo_reembolso_id')->references('id')->on('tipo_reembolso');
            $table->foreign('entidade_reembolso_id')->references('id')->on('entidade_reembolso');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reembolso');
    }
}
