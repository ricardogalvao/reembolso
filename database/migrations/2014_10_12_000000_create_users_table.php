<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('nome')->nullable()->default(null);
            $table->string('email')->nullable()->default(null)->unique();
            $table->string('cpf')->nullable()->default(null)->unique();
            $table->string('logradouro')->nullable()->default(null);
            $table->string('numero')->nullable()->default(null);
            $table->string('complemento')->nullable()->default(null);
            $table->string('bairro')->nullable()->default(null);
            $table->string('cidade')->nullable()->default(null);
            $table->string('estado')->nullable()->default(null);
            $table->string('cep')->nullable()->default(null);
            $table->rememberToken();
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
