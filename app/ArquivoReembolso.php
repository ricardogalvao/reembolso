<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArquivoReembolso extends Model
{   
    protected $table = 'arquivo_reembolso';
    protected $fillable = [
        'arquivo',
        'reembolso_id'
    ];

    public function arquivos()
    {
    	return $this->belongsTo(Reembolso::class);
    }
}
