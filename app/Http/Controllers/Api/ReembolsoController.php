<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\reembolsoRequest;
use App\Reembolso;
use App\EntidadeReembolso;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReembolsoController extends Controller
{
    public function __construct(Reembolso $reembolso, EntidadeReembolso $entidadeReembolso)
    {
        $this->entidadeReembolso = $entidadeReembolso;
        $this->reembolso = $reembolso;
    }

    public function index(){
        $reembolsos = $this->entidadeReembolso->with('reembolso')->paginate(10);

        return response()->json($reembolsos, 200);
    }

    public function show($id)
    {
        try {
    		$reembolso = $this->reembolso->with([
                'arquivos',
                'tipo',
                'entidadeReembolso',
            ])->findOrFail($id);

    		return response()->json([
    			'data' => [
    				'data' => $reembolso
    			]
    		], 200);

    	} catch (Exception $e) {
    		$message = new ApiMessages($e->getMessage());
    		response()->json($message->getMessage(), 401);
    	}
    }

    public function store(Request $request){
        $data = $request->all();
        $reembolsos = $request->all();

        // Converte Parametro em Objeto
        $convert_date_reembolso =  Carbon::parse($reembolsos['date']);
        $convert_createdAt =  Carbon::parse($data['createdAt']);
        
        // Converte para o Padrão datetime
        $reembolsos['date'] = Carbon::parse($reembolsos['date'])->toDateString();
        try {
            $entidadeReembolso = $this->entidadeReembolso->create($data); 
            $entidadeReembolso->reembolso()->create(                [
                    'date' => $reembolsos['date'],
                    'type' => $reembolsos['type'],
                    'description' => $reembolsos['description'],
                    'value' => $reembolsos['value'],
                    'timezone' => $reembolsos['timezone'],
                ]
            );

                return response()->json([
                    'data' => [
                        'msg' => 'Reembolso cadastrado com sucesso'
                    ]
                ], 200);
            } catch (Exception $e) {
            $message = new ApiMessages($e->getMessage());
            response()->json($message->getMessage(), 401);
            }
    }

    public function update(reembolsoRequest $request, $id){

        $data['value'] = $request['value'];

        try {
           
            $reembolso = $this->reembolso->findOrFail($id);
            $reembolso->update($data);
            return response()->json([
                'data' => [
                    'msg' => 'Reembolso atualizado com sucesso'
                ]
            ], 200);
            
        } catch (Exception $e) {
            $message = new ApiMessages($e->getMessage());
            response()->json($message->getMessage(), 401);
        }
    }

    public function destroy($id){  

        try {

            $reembolso = $this->reembolso->findOrFail($id);
            $reembolso->delete();

            return response()->json([
                'data' => [
                    'msg' => 'Reembolso excluido com sucesso'
                ]
            ], 200);
            
        } catch (Exception $e) {
            $message = new ApiMessages($e->getMessage());
            response()->json($message->getMessage(), 401);
        }
        
    }

    public function uploadPhoto(Request $request, $id){
        $image = $request->file('image');

        if($image){
            $imageUpload = [];
            $reembolso = $this->reembolso->findOrFail($id);
            $path = $image->store('images', 'public');

            $imageUpload = ['reembolso_id' => $reembolso->id, 'image' => $path];


            $reembolso->photos()->create($imageUpload);

            

            return response()->json([
                'data' => [
                    'msg' => 'Imagem cadastrada com sucesso'
                ]
            ], 200);
        }
        else{
            return ['msg' => 'É necessario inserir uma imagem', 401];  
        }
    }

    public function relatorio(Request $request){

        $report = DB::table('reembolsos')
                ->select(
                    DB::raw('MONTH(date) as month, YEAR(date) as year'),
                    DB::raw('sum(value) as totalreembolsos'),
                    DB::raw('count(id) as reembolsos')
                )
                ->whereYear('date', $request->year)
                ->whereMonth('date', $request->month)
                ->whereNull('deleted_at')
                ->groupBy(DB::raw('MONTH(date)'))
                ->get();

                return response()->json($report);

    }    

}
