<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Reembolso extends Model
{   
    use SoftDeletes;

    public $timestamps = false;
    protected $fillable = [
        'tipo_reembolso_id',
        'entidade_reembolso_id',
        'user_id',
        'descricao',
        'data',
        'valor',
        'valor_final',
        'valor_inicial',
    ];
    protected $dates = ['deleted_at'];

    public function entidadeReembolso(){
    	return $this->hasOne(EntidadeReembolso::class);
    }
    
    public function arquivos()
    {
    	return $this->hasMany(ArquivoReembolso::class, 'reembolso_id', 'id');
    }

    public function tipo(){
        return $this->belongsTo(TipoReembolso::class, 'reembolso_id');
    }

}
