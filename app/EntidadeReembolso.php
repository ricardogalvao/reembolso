<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntidadeReembolso extends Model
{   
    protected $table = 'entidade_reembolso';
    public $timestamps = false;
    protected $fillable = [
        'nome',
        'email',
        'cpf_cnpj',
        'logradouro',
        'numero',
        'cep',
        'complemento',
        'bairro',
        'cidade',
        'estado',
    ];

    public function reembolso(){
        return $this->hasOne(Reembolso::class);
    }
}
