<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoReembolso extends Model
{   
    protected $table = 'tipo_reembolso';
    protected $fillable = [
        'nome',
        'reembolso_id'
    ];
    
    public function reembolso()
    {
        return $this->belongsTo(Reembolso::class, 'reembolso_id');
    }
}
